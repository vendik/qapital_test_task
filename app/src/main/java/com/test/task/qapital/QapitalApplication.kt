package com.test.task.qapital

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class QapitalApplication : Application() {
}