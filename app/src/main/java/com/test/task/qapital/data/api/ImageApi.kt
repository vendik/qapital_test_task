package com.test.task.qapital.data.api

import io.reactivex.rxjava3.core.Single
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Url

interface ImageApi {

    @GET
    fun getImage(@Url url: String): Single<ResponseBody>
}