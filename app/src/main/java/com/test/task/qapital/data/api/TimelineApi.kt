package com.test.task.qapital.data.api

import com.test.task.qapital.data.dto.TimelineDto
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TimelineApi {

    @GET("activities")
    fun getTimeline(@Query("from") from: String, @Query("to") to: String): Single<TimelineDto>
}