package com.test.task.qapital.data.api

import com.test.task.qapital.data.dto.UserDto
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface UserApi {

    @GET("users/{id}")
    fun getUser(@Path("id") id: Long): Single<UserDto>
}