package com.test.task.qapital.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.task.qapital.data.dto.ImageDbDto
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
interface ImageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addImage(imageDbDto: ImageDbDto): Completable

    @Query("SELECT * FROM image WHERE url = :url")
    fun getImage(url: String): Single<ImageDbDto>
}