package com.test.task.qapital.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.test.task.qapital.data.dto.ImageDbDto

@Database(entities = [ImageDbDto::class], exportSchema = true, version = 1)
abstract class ImageDatabase : RoomDatabase() {
    companion object {
        private const val DATABASE_NAME = "image.db"

        fun buildDatabase(context: Context): ImageDatabase {
            return Room.databaseBuilder(context.applicationContext, ImageDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun imageDao(): ImageDao
}