package com.test.task.qapital.data.di

import com.test.task.qapital.data.api.ImageApi
import com.test.task.qapital.data.api.TimelineApi
import com.test.task.qapital.data.api.UserApi
import com.test.task.qapital.data.repository.ImageRepository
import com.test.task.qapital.data.repository.TimelineRepository
import com.test.task.qapital.data.repository.UserRepository
import com.test.task.qapital.domain.datasource.ImageDataSource
import com.test.task.qapital.domain.datasource.TimelineDataSource
import com.test.task.qapital.domain.datasource.UserDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    @Singleton
    abstract fun provideImageDataSource(imageRepository: ImageRepository): ImageDataSource

    @Binds
    @Singleton
    abstract fun provideTimelineDataSource(timelineRepository: TimelineRepository): TimelineDataSource

    @Binds
    @Singleton
    abstract fun provideUserDataSource(userRepository: UserRepository): UserDataSource
}