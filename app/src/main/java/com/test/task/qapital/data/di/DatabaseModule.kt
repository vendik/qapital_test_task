package com.test.task.qapital.data.di

import android.content.Context
import com.test.task.qapital.data.database.ImageDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun provideImageDao(database: ImageDatabase) =
        database.imageDao()

    @Provides
    @Singleton
    fun provideImageDatabase(@ApplicationContext context: Context) =
        ImageDatabase.buildDatabase(context)
}