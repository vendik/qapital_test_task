package com.test.task.qapital.data.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "image")
data class ImageDbDto(
    @PrimaryKey(autoGenerate = false) val url: String,
    val bytes: ByteArray
)