package com.test.task.qapital.data.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class TimelineDto(
    @JsonProperty("activities") val items: List<TimelineItemDto>,
    @JsonProperty("oldest") val oldestTime: String
)

data class TimelineItemDto(
    @JsonProperty("amount") val amount: Number,
    @JsonProperty("message") val message: String,
    @JsonProperty("timestamp") val time: String,
    @JsonProperty("userId") val userId: Long
)