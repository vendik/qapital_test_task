package com.test.task.qapital.data.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class UserDto(
    @JsonProperty("avatarUrl") val avatarUrl: String,
    @JsonProperty("displayName") val displayName: String,
    @JsonProperty("userId") val id: Long
)