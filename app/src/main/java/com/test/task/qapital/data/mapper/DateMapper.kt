package com.test.task.qapital.data.mapper

import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DateMapper @Inject constructor() {

    companion object {
        private const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
    }

    fun map(date: String): Date? {
        val simpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.ROOT)
        return simpleDateFormat.parse(date)
    }

    fun reverseMap(date: Date): String {
        val simpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.ROOT)
        return simpleDateFormat.format(date)
    }
}