package com.test.task.qapital.data.mapper

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.test.task.qapital.data.dto.ImageDbDto
import java.io.ByteArrayOutputStream
import javax.inject.Inject

class ImageDbMapper @Inject constructor() {

    fun map(imageDbDto: ImageDbDto): Bitmap {
        return BitmapFactory.decodeByteArray(imageDbDto.bytes,0, imageDbDto.bytes.size);
    }

    fun reverseMap(url: String, bitmap: Bitmap): ImageDbDto {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)

        return ImageDbDto(
            url = url,
            bytes = byteArrayOutputStream.toByteArray()
        )
    }
}