package com.test.task.qapital.data.mapper

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import okhttp3.ResponseBody
import javax.inject.Inject

class ImageMapper @Inject constructor() {

    fun map(responseBody: ResponseBody): Bitmap {
        return BitmapFactory.decodeStream(responseBody.byteStream());
    }
}