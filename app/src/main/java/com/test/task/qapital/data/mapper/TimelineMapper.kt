package com.test.task.qapital.data.mapper

import com.test.task.qapital.data.dto.TimelineDto
import com.test.task.qapital.data.dto.TimelineItemDto
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import javax.inject.Inject

class TimelineMapper @Inject constructor(
    private val dateMapper: DateMapper,
    private val timelineItemMapper: TimelineItemMapper
) {

    fun map(timelineDto: TimelineDto): Timeline {
        return Timeline(
            items = timelineDto.items.map { timelineItemMapper.map(it) },
            oldestTime = dateMapper.map(timelineDto.oldestTime)!!
        )
    }
}

class TimelineItemMapper @Inject constructor(
    private val dateMapper: DateMapper
) {

    fun map(timelineItemDto: TimelineItemDto): TimelineItem {
        return TimelineItem(
            amount = timelineItemDto.amount,
            message= timelineItemDto.message,
            time = dateMapper.map(timelineItemDto.time)!!,
            user = null,
            userId = timelineItemDto.userId
        )
    }
}