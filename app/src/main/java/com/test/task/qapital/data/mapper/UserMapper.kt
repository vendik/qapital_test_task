package com.test.task.qapital.data.mapper

import com.test.task.qapital.data.dto.UserDto
import com.test.task.qapital.domain.entity.User
import javax.inject.Inject

class UserMapper @Inject constructor() {

    fun map(userDto: UserDto): User {
        return User(
            avatar = null,
            avatarUrl = userDto.avatarUrl,
            displayName = userDto.displayName,
            id = userDto.id
        )
    }
}