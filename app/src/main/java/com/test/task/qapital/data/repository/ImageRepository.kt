package com.test.task.qapital.data.repository

import android.graphics.Bitmap
import android.util.Log
import com.test.task.qapital.data.api.ImageApi
import com.test.task.qapital.data.database.ImageDao
import com.test.task.qapital.data.mapper.ImageDbMapper
import com.test.task.qapital.data.mapper.ImageMapper
import com.test.task.qapital.domain.datasource.ImageDataSource
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ImageRepository @Inject constructor(
    private val imageApi: ImageApi,
    private val imageDao: ImageDao,
    private val imageDbMapper: ImageDbMapper,
    private val imageMapper: ImageMapper
) : ImageDataSource {

    override fun getImage(url: String): Single<Bitmap> {
        return getImageOffline(url)
    }

    private fun getImageOffline(url: String): Single<Bitmap> {
        return imageDao.getImage(url)
            .map(imageDbMapper::map)
            .onErrorResumeNext { getImageOnline(url) }
    }

    private fun getImageOnline(url: String): Single<Bitmap> {
        return imageApi.getImage(url)
            .map(imageMapper::map)
            .flatMap { cacheImageOffline(url, it) }
    }

    private fun cacheImageOffline(url: String, bitmap: Bitmap): Single<Bitmap> {
        return imageDao.addImage(imageDbMapper.reverseMap(url, bitmap))
            .toSingleDefault(bitmap)
    }
}