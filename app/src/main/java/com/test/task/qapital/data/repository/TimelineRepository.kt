package com.test.task.qapital.data.repository

import com.test.task.qapital.data.api.TimelineApi
import com.test.task.qapital.data.mapper.DateMapper
import com.test.task.qapital.data.mapper.TimelineMapper
import com.test.task.qapital.domain.datasource.TimelineDataSource
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import io.reactivex.rxjava3.core.Single
import java.util.*
import javax.inject.Inject

class TimelineRepository @Inject constructor(
    private val dateMapper: DateMapper,
    private val timelineApi: TimelineApi,
    private val timelineMapper: TimelineMapper
) : TimelineDataSource {

    private val timelineItems = mutableMapOf<Date, TimelineItem>()

    override fun getTimeline(fromTime: Date, toTime: Date): Single<Timeline> {
        return timelineApi.getTimeline(dateMapper.reverseMap(fromTime), dateMapper.reverseMap(toTime))
            .map(timelineMapper::map)
            .map(this::cacheTimelineMemory)
    }

    private fun cacheTimelineMemory(timeline: Timeline): Timeline {
        timeline.items.forEach { timelineItem ->
            timelineItems[timelineItem.time] = timelineItem
        }

        val timelineItemsList = timelineItems.values
            .sortedWith(compareByDescending { timelineItem -> timelineItem.time })

        return timeline.copy(items = timelineItemsList)
    }
}