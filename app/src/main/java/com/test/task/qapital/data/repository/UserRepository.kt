package com.test.task.qapital.data.repository

import android.util.Log
import com.test.task.qapital.data.api.UserApi
import com.test.task.qapital.data.mapper.UserMapper
import com.test.task.qapital.domain.datasource.UserDataSource
import com.test.task.qapital.domain.entity.User
import io.reactivex.rxjava3.core.Single
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userApi: UserApi,
    private val userMapper: UserMapper
) : UserDataSource {

    private val users = ConcurrentHashMap<Long, User>()

    override fun getUser(id: Long): Single<User> {
        return getUserMemory(id)
    }

    private fun getUserMemory(id: Long): Single<User> {
        return Single.fromCallable { users[id]!! }
            .onErrorResumeNext { getUserOnline(id) }
    }

    private fun getUserOnline(id: Long): Single<User> {
        return userApi.getUser(id)
            .map(userMapper::map)
            .map { cacheUserMemory(id, it) }
    }

    private fun cacheUserMemory(id: Long, user: User): User {
        users[id] = user
        return user
    }
}