package com.test.task.qapital.domain.datasource

import android.graphics.Bitmap
import io.reactivex.rxjava3.core.Single

interface ImageDataSource {
    fun getImage(url: String): Single<Bitmap>
}