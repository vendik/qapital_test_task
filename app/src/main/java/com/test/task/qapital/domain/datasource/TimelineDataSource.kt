package com.test.task.qapital.domain.datasource

import com.test.task.qapital.domain.entity.Timeline
import io.reactivex.rxjava3.core.Single
import java.util.*

interface TimelineDataSource {
    fun getTimeline(fromTime: Date, toTime: Date): Single<Timeline>
}