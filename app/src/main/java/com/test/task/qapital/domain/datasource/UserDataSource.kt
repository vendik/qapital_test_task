package com.test.task.qapital.domain.datasource

import com.test.task.qapital.domain.entity.User
import io.reactivex.rxjava3.core.Single

interface UserDataSource {
    fun getUser(id: Long): Single<User>
}