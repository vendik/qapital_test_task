package com.test.task.qapital.domain.entity

import java.util.*

data class Timeline(
    val items: List<TimelineItem>,
    val oldestTime: Date
)

data class TimelineItem(
    val amount: Number,
    val message: String,
    val time: Date,
    val user: User?,
    val userId: Long
)