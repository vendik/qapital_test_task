package com.test.task.qapital.domain.entity

import android.graphics.Bitmap

data class User(
    val avatar: Bitmap?,
    val avatarUrl: String,
    val displayName: String,
    val id: Long
)