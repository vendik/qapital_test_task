package com.test.task.qapital.domain.interactor

import com.test.task.qapital.domain.datasource.ImageDataSource
import com.test.task.qapital.domain.datasource.TimelineDataSource
import com.test.task.qapital.domain.datasource.UserDataSource
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import com.test.task.qapital.domain.entity.User
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import java.util.Date
import javax.inject.Inject

class TimelineInteractor @Inject constructor(
    private val imageDataSource: ImageDataSource,
    private val timelineDataSource: TimelineDataSource,
    private val userDataSource: UserDataSource
) {
    fun getTimeline(fromTime: Date, toTime: Date): Single<Timeline> {
        return timelineDataSource.getTimeline(fromTime, toTime)
            .flatMap(
                { timeline -> resolveTimelineItems(timeline.items) },
                { timeline, timelineItems -> timeline.copy(items = timelineItems)}
            )
    }

    private fun resolveTimelineItems(timelineItems: List<TimelineItem>): Single<List<TimelineItem>> {
        return Observable.fromIterable(timelineItems)
            .concatMap { timelineItem ->
                resolveUser(timelineItem.userId)
                    .map { user -> timelineItem.copy(user = user) }
                    .toObservable()
            }
            .toList()
    }

    private fun resolveUser(userId: Long): Single<User> {
        return userDataSource.getUser(userId)
            .flatMap(
                { imageDataSource.getImage(it.avatarUrl) },
                { user, image -> user.copy(avatar = image) }
            )
    }
}