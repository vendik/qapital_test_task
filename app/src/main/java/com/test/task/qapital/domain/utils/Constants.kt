package com.test.task.qapital.domain.utils

class Constants {
    object Url {
        const val BASE_URL = "https://qapital-ios-testtask.herokuapp.com"
    }
}