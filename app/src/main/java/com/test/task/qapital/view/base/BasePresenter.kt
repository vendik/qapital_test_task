package com.test.task.qapital.view.base

import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BasePresenter<T: BaseView> {
    protected val disposable = CompositeDisposable()

    protected var view: T? = null

    fun attach(view: T) {
        this.view = view
    }

    fun detach() {
        disposable.dispose()
    }
}