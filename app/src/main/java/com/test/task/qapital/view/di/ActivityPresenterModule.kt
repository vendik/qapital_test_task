package com.test.task.qapital.view.di

import com.test.task.qapital.data.repository.ImageRepository
import com.test.task.qapital.domain.datasource.ImageDataSource
import com.test.task.qapital.view.timeline.TimelineContract
import com.test.task.qapital.view.timeline.TimelinePresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Singleton

@Module
@InstallIn(ActivityComponent::class)
abstract class ActivityPresenterModule {
    @Binds
    @ActivityScoped
    abstract fun provideTimelinePresenter(timelinePresenter: TimelinePresenter): TimelineContract.Presenter
}