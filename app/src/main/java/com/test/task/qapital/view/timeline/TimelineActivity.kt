package com.test.task.qapital

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.test.task.qapital.databinding.ActivityTimelineBinding
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import com.test.task.qapital.view.timeline.TimelineAdapter
import com.test.task.qapital.view.timeline.TimelineContract
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TimelineActivity : AppCompatActivity(), TimelineContract.View {

    private lateinit var binding: ActivityTimelineBinding

    @Inject
    protected lateinit var presenter: TimelineContract.Presenter

    private val timelineAdapter = TimelineAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTimelineBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.list.adapter = timelineAdapter
        binding.list.layoutManager = LinearLayoutManager(this)
        binding.list.setOnScrollChangeListener { _, _, _, _, _ -> timelineScrolled() }

        binding.refresh.setOnRefreshListener { presenter.refreshTimeline() }

        presenter.attach(this)
        presenter.refreshTimeline()
    }

    override fun onDestroy() {
        presenter.detach()

        super.onDestroy()
    }

    override fun onError(throwable: Throwable) {
        Snackbar.make(binding.root, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show();
    }

    override fun onLoading(loading: Boolean) {
        binding.refresh.isRefreshing = loading
    }

    override fun onTimeline(timeline: Timeline) {
        timelineAdapter.setTimeline(timeline)
        timelineScrolled()
    }

    private fun timelineScrolled() {
        val layoutManager = binding.list.layoutManager as LinearLayoutManager
        val lastVisibleTimelineItem = layoutManager.findLastVisibleItemPosition()
        presenter.timelineScrolled(lastVisibleTimelineItem, timelineAdapter.timeline!!)
    }
}