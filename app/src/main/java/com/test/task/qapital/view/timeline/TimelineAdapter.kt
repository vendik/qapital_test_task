package com.test.task.qapital.view.timeline

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import com.test.task.qapital.databinding.ListItemTimelineBinding
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class TimelineAdapter : RecyclerView.Adapter<TimelineAdapter.ViewHolder>() {

    companion object {
        private const val DATE_FORMAT = "dd MMM yyyy";
    }

    private val decimalFormat = DecimalFormat("$0.00")
    private val simpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.ROOT)

    private val sortedList = SortedList(TimelineItem::class.java, SortedListCallback())

    var timeline: Timeline? = null
        private set

    override fun getItemCount() =
        sortedList.size()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(sortedList.get(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ListItemTimelineBinding.inflate(inflater, parent, false))
    }

    fun setTimeline(timeline: Timeline) {
        this.timeline = timeline
        sortedList.beginBatchedUpdates()
        sortedList.addAll(timeline.items)
        sortedList.endBatchedUpdates()
    }

    inner class ViewHolder(
        private val binding: ListItemTimelineBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(timelineItem: TimelineItem) {
            binding.amount.text = decimalFormat.format(timelineItem.amount)
            binding.avatar.setImageBitmap(timelineItem.user?.avatar)
            binding.message.text = HtmlCompat.fromHtml(timelineItem.message, HtmlCompat.FROM_HTML_MODE_LEGACY)
            binding.date.text = simpleDateFormat.format(timelineItem.time)
        }
    }

    private inner class SortedListCallback : SortedList.Callback<TimelineItem>() {

        override fun areContentsTheSame(oldItem: TimelineItem?, newItem: TimelineItem?) =
            oldItem == newItem

        override fun areItemsTheSame(item1: TimelineItem?, item2: TimelineItem?) =
            item1!!.time == item2!!.time

        override fun compare(o1: TimelineItem?, o2: TimelineItem?) =
            o2!!.time.compareTo(o1!!.time)

        override fun onChanged(position: Int, count: Int) =
            notifyItemRangeChanged(position, count)

        override fun onInserted(position: Int, count: Int) =
            notifyItemRangeInserted(position, count)

        override fun onMoved(fromPosition: Int, toPosition: Int) =
            notifyItemMoved(fromPosition, toPosition)

        override fun onRemoved(position: Int, count: Int) =
            notifyItemRangeRemoved(position, count)
    }
}