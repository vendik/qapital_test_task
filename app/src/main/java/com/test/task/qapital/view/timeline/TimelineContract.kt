package com.test.task.qapital.view.timeline

import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import com.test.task.qapital.view.base.BasePresenter
import com.test.task.qapital.view.base.BaseView

interface TimelineContract {
    interface View: BaseView {
        fun onError(throwable: Throwable)
        fun onLoading(loading: Boolean)
        fun onTimeline(timeline: Timeline)
    }

    abstract class Presenter: BasePresenter<View>() {
        abstract fun refreshTimeline();
        abstract fun timelineScrolled(lastVisibleTimelineItem: Int, timeline: Timeline)
    }
}