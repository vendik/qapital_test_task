package com.test.task.qapital.view.timeline

import android.util.Log
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.interactor.TimelineInteractor
import com.test.task.qapital.domain.utils.add
import com.test.task.qapital.domain.utils.addTo
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class TimelinePresenter @Inject constructor(
    private val timelineInteractor: TimelineInteractor
) : TimelineContract.Presenter() {

    companion object {
        private const val FETCH_PERIOD_DAYS = 14

        private const val FETCH_THRESHOLD_ITEMS_LEFT = 5
    }

    private var loading = false

    private var oldestFetchTime = Date()

    override fun refreshTimeline() {
        val toTime = Date()
        val fromTime = toTime.add(-FETCH_PERIOD_DAYS)
        fetchTimeline(fromTime, toTime)
    }

    override fun timelineScrolled(lastVisibleTimelineItem: Int, timeline: Timeline) {
        val hasMoreData = oldestFetchTime > timeline.oldestTime
        val scrolledToTheEnd = lastVisibleTimelineItem >= timeline.items.size - FETCH_THRESHOLD_ITEMS_LEFT

        if (scrolledToTheEnd && hasMoreData) {
            val toTime = oldestFetchTime
            val fromTime = toTime.add(-FETCH_PERIOD_DAYS)
            fetchTimeline(fromTime, toTime)
        }
    }

    private fun fetchTimeline(fromTime: Date, toTime: Date) {
        if (!loading) {
            loading = true
            view?.onLoading(loading)

            timelineInteractor.getTimeline(fromTime, toTime)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { timeline -> fetchTimelineSuccess(fromTime, timeline)},
                    { throwable -> fetchTimelineError(throwable)}
                )
                .addTo(disposable)
        }
    }

    private fun fetchTimelineSuccess(fromTime: Date, timeline: Timeline) {
        oldestFetchTime = minOf(oldestFetchTime, fromTime)

        loading = false

        view?.onLoading(loading)
        view?.onTimeline(timeline)
    }

    private fun fetchTimelineError(throwable: Throwable) {
        loading = false

        view?.onLoading(loading)
        view?.onError(throwable)
    }
}