package com.test.task.qapital.data.repository

import android.graphics.Bitmap
import com.nhaarman.mockitokotlin2.*
import com.test.task.qapital.data.api.ImageApi
import com.test.task.qapital.data.database.ImageDao
import com.test.task.qapital.data.dto.ImageDbDto
import com.test.task.qapital.data.mapper.ImageDbMapper
import com.test.task.qapital.data.mapper.ImageMapper
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.observers.TestObserver
import junit.framework.Assert.assertEquals
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.robolectric.RobolectricTestRunner
import java.io.ByteArrayOutputStream

@RunWith(RobolectricTestRunner::class)
class ImageRepositoryTest {
    private val imageApi = mock<ImageApi>()
    private val imageDao = mock<ImageDao>()
    private val imageDbMapper = ImageDbMapper()
    private val imageMapper = ImageMapper()

    @Test
    fun testFailure() {
        val imageRepository = ImageRepository(imageApi, imageDao, imageDbMapper, imageMapper)

        val imageUrl ="https://bitmap"

        val apiError = Throwable()
        whenever(imageApi.getImage(any())).thenReturn(Single.error(apiError))

        val daoError = Throwable()
        whenever(imageDao.getImage(any())).thenReturn(Single.error(daoError))

        // Make sure there were no errors and the correct bitmap was returned
        val testObserver = TestObserver<Bitmap>()
        imageRepository.getImage(imageUrl).subscribe(testObserver)
        testObserver.assertError(apiError)
        testObserver.assertNotComplete()
        testObserver.assertValueCount(0)

        // Make sure image was not cached
        argumentCaptor<ImageDbDto>().apply {
            verify(imageDao, times(0)).addImage(capture())
        }
    }

    @Test
    fun testSuccessOffline() {
        val imageRepository = ImageRepository(imageApi, imageDao, imageDbMapper, imageMapper)

        val imageUrl ="https://bitmap"

        val offlineBitmapSize = 2
        val offlineBitmap = ImageDbDto(url = imageUrl, bytes = createCompressedBitmap(offlineBitmapSize))
        whenever(imageDao.getImage(anyString())).thenReturn(Single.just(offlineBitmap))

        // Make sure there were no errors and the correct bitmap was returned
        val testObserver = TestObserver<Bitmap>()
        imageRepository.getImage(imageUrl).subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertValue { bitmap ->
            val bitmapSize = getBitmapSize(bitmap)
            bitmapSize == offlineBitmapSize
        }

        // Make sure api was not invoked
        argumentCaptor<String>().apply {
            verify(imageApi, times(0)).getImage(anyOrNull())
        }
    }

    @Test
    fun testSuccessOnline() {
        val imageRepository = ImageRepository(imageApi, imageDao, imageDbMapper, imageMapper)

        val imageUrl ="https://bitmap"

        val onlineBitmapSize = 3
        val onlineBitmap = createCompressedBitmap(onlineBitmapSize).toResponseBody()
        whenever(imageApi.getImage(any())).thenReturn(Single.just(onlineBitmap))

        whenever(imageDao.getImage(any())).thenReturn(Single.error(Throwable()))
        whenever(imageDao.addImage(any())).thenReturn(Completable.complete())

        // Make sure there were no errors and the correct bitmap was returned
        val testObserver = TestObserver<Bitmap>()
        imageRepository.getImage(imageUrl).subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertValue { bitmap ->
            val bitmapSize = getBitmapSize(bitmap)
            bitmapSize == onlineBitmapSize
        }

        // Make sure image was cached
        argumentCaptor<ImageDbDto>().apply {
            verify(imageDao, times(1)).addImage(capture())
            assertEquals(firstValue.url, imageUrl)
        }
    }

    private fun createCompressedBitmap(size: Int): ByteArray {
        val bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)

        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)

        return byteArrayOutputStream.toByteArray()
    }

    private fun getBitmapSize(bitmap: Bitmap): Int {
        return bitmap.width
    }
}