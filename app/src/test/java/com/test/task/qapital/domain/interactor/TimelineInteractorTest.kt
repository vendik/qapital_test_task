package com.test.task.qapital.domain.interactor

import android.graphics.Bitmap
import com.nhaarman.mockitokotlin2.*
import com.test.task.qapital.data.dto.ImageDbDto
import com.test.task.qapital.data.repository.ImageRepository
import com.test.task.qapital.domain.datasource.ImageDataSource
import com.test.task.qapital.domain.datasource.TimelineDataSource
import com.test.task.qapital.domain.datasource.UserDataSource
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import com.test.task.qapital.domain.entity.User
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.observers.TestObserver
import junit.framework.Assert
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.*

@RunWith(RobolectricTestRunner::class)
class TimelineInteractorTest {
    private val imageDataSource = mock<ImageDataSource>()
    private val timelineDataSource = mock<TimelineDataSource>()
    private val userDataSource = mock<UserDataSource>()

    @Test
    fun testFailureMissingImage() {
        val timelineInteractor = TimelineInteractor(imageDataSource, timelineDataSource, userDataSource)

        val imageError = Throwable()
        whenever(imageDataSource.getImage(any())).thenReturn(Single.error(imageError))

        val userId = 1337L
        val user = User(null, "", "", userId)
        whenever(userDataSource.getUser(any())).thenReturn(Single.just(user))

        val timelineItem = TimelineItem(amount = 0, message = "", time = Date(), user = null, userId = userId)
        val timeline = Timeline(items = listOf(timelineItem), oldestTime = Date())
        whenever(timelineDataSource.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        // Make sure there were no errors and the correct bitmap was returned
        val testObserver = TestObserver<Timeline>()
        timelineInteractor.getTimeline(Date(), Date()).subscribe(testObserver)
        testObserver.assertError(imageError)
    }

    @Test
    fun testFailureMissingTimeline() {
        val timelineInteractor = TimelineInteractor(imageDataSource, timelineDataSource, userDataSource)

        val bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        whenever(imageDataSource.getImage(any())).thenReturn(Single.just(bitmap))

        val userId = 1337L
        val user = User(null, "", "", userId)
        whenever(userDataSource.getUser(any())).thenReturn(Single.just(user))

        val timelineError = Throwable()
        whenever(timelineDataSource.getTimeline(any(), any())).thenReturn(Single.error(timelineError))

        // Make sure there were no errors and the correct bitmap was returned
        val testObserver = TestObserver<Timeline>()
        timelineInteractor.getTimeline(Date(), Date()).subscribe(testObserver)
        testObserver.assertError(timelineError)
    }

    @Test
    fun testFailureMissingUser() {
        val timelineInteractor = TimelineInteractor(imageDataSource, timelineDataSource, userDataSource)

        val bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        whenever(imageDataSource.getImage(any())).thenReturn(Single.just(bitmap))

        val userError = Throwable()
        whenever(userDataSource.getUser(any())).thenReturn(Single.error(userError))

        val timelineItem = TimelineItem(amount = 0, message = "", time = Date(), user = null, userId = 1337)
        val timeline = Timeline(items = listOf(timelineItem), oldestTime = Date())
        whenever(timelineDataSource.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        // Make sure there were no errors and the correct bitmap was returned
        val testObserver = TestObserver<Timeline>()
        timelineInteractor.getTimeline(Date(), Date()).subscribe(testObserver)
        testObserver.assertError(userError)
    }

    @Test
    fun testSuccess() {
        val timelineInteractor = TimelineInteractor(imageDataSource, timelineDataSource, userDataSource)

        val bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        whenever(imageDataSource.getImage(any())).thenReturn(Single.just(bitmap))

        val userId = 1337L
        val user = User(null, "", "", userId)
        whenever(userDataSource.getUser(any())).thenReturn(Single.just(user))

        val timelineItem = TimelineItem(amount = 0, message = "", time = Date(), user = null, userId = userId)
        val timeline = Timeline(items = listOf(timelineItem), oldestTime = Date())
        whenever(timelineDataSource.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        // Make sure there were no errors and the correct bitmap was returned
        val testObserver = TestObserver<Timeline>()
        timelineInteractor.getTimeline(Date(), Date()).subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first()
        assertEquals(result.items.size, timeline.items.size)
        assertEquals(result.items.first().user!!.id, user.id)
        assertEquals(result.items.first().user!!.avatar, bitmap)
    }
}

