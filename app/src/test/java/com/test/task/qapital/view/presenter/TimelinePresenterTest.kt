package com.test.task.qapital.view.presenter

import android.os.Looper.getMainLooper
import com.nhaarman.mockitokotlin2.*
import com.test.task.qapital.domain.entity.Timeline
import com.test.task.qapital.domain.entity.TimelineItem
import com.test.task.qapital.domain.interactor.TimelineInteractor
import com.test.task.qapital.view.timeline.TimelineContract
import com.test.task.qapital.view.timeline.TimelinePresenter
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import junit.framework.Assert.assertEquals
import net.bytebuddy.implementation.bytecode.Throw
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows.shadowOf
import java.util.*

@RunWith(RobolectricTestRunner::class)
class TimelinePresenterTest {
    private val timelineInteractor = mock<TimelineInteractor>()
    private val timelineView = mock<TimelineContract.View>()

    @Before
    fun before() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun testRefreshFailure() {
        val timelinePresenter = TimelinePresenter(timelineInteractor)
        timelinePresenter.attach(timelineView)

        val timelineError = Throwable()
        whenever(timelineInteractor.getTimeline(any(), any())).thenReturn(Single.error(timelineError))

        timelinePresenter.refreshTimeline()

        // Make sure the loading state was propagated
        argumentCaptor<Boolean>().apply {
            verify(timelineView, times(2)).onLoading(capture())
            assertEquals(firstValue, true)
            assertEquals(secondValue, false)
        }

        // Make sure the error was propagated
        argumentCaptor<Throwable>().apply {
            verify(timelineView, times(1)).onError(capture())
            assertEquals(firstValue, timelineError)
        }

        // Make sure there was no timeline
        argumentCaptor<Timeline>().apply {
            verify(timelineView, times(0)).onTimeline(any())
        }
    }

    @Test
    fun testRefreshSuccess() {
        val timelinePresenter = TimelinePresenter(timelineInteractor)
        timelinePresenter.attach(timelineView)

        val timeline = Timeline(items = listOf(), oldestTime = Date())
        whenever(timelineInteractor.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        timelinePresenter.refreshTimeline()

        // Make sure the loading state was propagated
        argumentCaptor<Boolean>().apply {
            verify(timelineView, times(2)).onLoading(capture())
            assertEquals(firstValue, true)
            assertEquals(secondValue, false)
        }

        // Make sure timeline was propagated
        argumentCaptor<Timeline>().apply {
            verify(timelineView, times(1)).onTimeline(capture())
            assertEquals(firstValue, timeline)
        }

        // Make sure there was no error
        argumentCaptor<Throwable>().apply {
            verify(timelineView, times(0)).onError(any())
        }
    }

    @Test
    fun testScrollNotEndHasData() {
        val timelinePresenter = TimelinePresenter(timelineInteractor)
        timelinePresenter.attach(timelineView)

        val timelineItemCount = 50
        val lastVisibleTimeLineItem = 15
        val oldestTime = Date(0L)

        val timeline = Timeline(items = createTimelineItemList(timelineItemCount), oldestTime = oldestTime)
        whenever(timelineInteractor.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        timelinePresenter.timelineScrolled(lastVisibleTimeLineItem, timeline)

        // Make sure the loading state was not propagated
        argumentCaptor<Boolean>().apply {
            verify(timelineView, times(0)).onLoading(capture())
        }

        // Make sure timeline was not propagated
        argumentCaptor<Timeline>().apply {
            verify(timelineView, times(0)).onTimeline(capture())
        }

        // Make sure there was no error
        argumentCaptor<Throwable>().apply {
            verify(timelineView, times(0)).onError(any())
        }
    }

    @Test
    fun testScrollNotEndNoData() {
        val timelinePresenter = TimelinePresenter(timelineInteractor)
        timelinePresenter.attach(timelineView)

        val timelineItemCount = 50
        val lastVisibleTimeLineItem = 15
        val oldestTime = Date(Long.MAX_VALUE)

        val timeline = Timeline(items = createTimelineItemList(timelineItemCount), oldestTime = oldestTime)
        whenever(timelineInteractor.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        timelinePresenter.timelineScrolled(lastVisibleTimeLineItem, timeline)

        // Make sure the loading state was not propagated
        argumentCaptor<Boolean>().apply {
            verify(timelineView, times(0)).onLoading(capture())
        }

        // Make sure timeline was not propagated
        argumentCaptor<Timeline>().apply {
            verify(timelineView, times(0)).onTimeline(capture())
        }

        // Make sure there was no error
        argumentCaptor<Throwable>().apply {
            verify(timelineView, times(0)).onError(any())
        }
    }

    @Test
    fun testScrollEndHasData() {
        val timelinePresenter = TimelinePresenter(timelineInteractor)
        timelinePresenter.attach(timelineView)

        val timelineItemCount = 50
        val lastVisibleTimeLineItem = 50
        val oldestTime = Date(0L)

        val timeline = Timeline(items = createTimelineItemList(timelineItemCount), oldestTime = oldestTime)
        whenever(timelineInteractor.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        timelinePresenter.timelineScrolled(lastVisibleTimeLineItem, timeline)

        // Make sure the loading state was propagated
        argumentCaptor<Boolean>().apply {
            verify(timelineView, times(2)).onLoading(capture())
            assertEquals(firstValue, true)
            assertEquals(secondValue, false)
        }

        // Make sure timeline was propagated
        argumentCaptor<Timeline>().apply {
            verify(timelineView, times(1)).onTimeline(capture())
            assertEquals(firstValue, timeline)
        }

        // Make sure there was no error
        argumentCaptor<Throwable>().apply {
            verify(timelineView, times(0)).onError(any())
        }
    }

    @Test
    fun testScrollEndNoData() {
        val timelinePresenter = TimelinePresenter(timelineInteractor)
        timelinePresenter.attach(timelineView)

        val timelineItemCount = 50
        val lastVisibleTimeLineItem = 50
        val oldestTime = Date(Long.MAX_VALUE)

        val timeline = Timeline(items = createTimelineItemList(timelineItemCount), oldestTime = oldestTime)
        whenever(timelineInteractor.getTimeline(any(), any())).thenReturn(Single.just(timeline))

        timelinePresenter.timelineScrolled(lastVisibleTimeLineItem, timeline)

        // Make sure the loading state was not propagated
        argumentCaptor<Boolean>().apply {
            verify(timelineView, times(0)).onLoading(capture())
        }

        // Make sure timeline was not propagated
        argumentCaptor<Timeline>().apply {
            verify(timelineView, times(0)).onTimeline(capture())
        }

        // Make sure there was no error
        argumentCaptor<Throwable>().apply {
            verify(timelineView, times(0)).onError(any())
        }
    }

    private fun createTimelineItemList(size: Int): List<TimelineItem> {
        return List(size) { index ->
            TimelineItem(amount = 0, message = "", time = Date(), user = null, userId = index.toLong())
        }
    }
}

